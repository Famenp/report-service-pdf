<?php

$mysqli = mysqli_init();
$mysqli->real_connect('localhost', 'root', '', '',3306);
if ($mysqli->connect_errno) 
{
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}
else
{
  $mysqli->set_charset("utf8");
}
function str_format()
{
   $args = func_get_args();
   $str = mb_ereg_replace('{([0-9]+)}', '%\\1$s', array_shift($args));
   return vsprintf($str, array_values($args));
}

function sprintf_assoc( $string = '', $replacement_vars = array(), $prefix_character = '%' ) {
    if ( ! $string ) return '';
    if ( is_array( $replacement_vars ) && count( $replacement_vars ) > 0 ) {
        foreach ( $replacement_vars as $key => $value ) {
            $string = str_replace( $prefix_character . $key, $value, $string );
        }
    }
    return $string;
}
 
function printf_assoc( $string = '', $replacement_vars = array(), $prefix_character = '%' ) {
    echo sprintf_assoc( $string, $replacement_vars, $prefix_character );
}

function toArrayString($result,$ch)
{
  if($ch == 1)echo '[';
    $field = 0;
    $c = 0;
      while($obj = $result->fetch_object())
      { 
        $c++;
          if($field == 0)
          {
            $fieldName = array_keys(get_object_vars($obj));
            $numField = count($fieldName);
            $field = 1;
          }
          echo '[';
          for($i=0;$i<$numField;$i++)
          {
            // if (is_numeric ($obj->{$fieldName[$i]})) echo  $obj->{$fieldName[$i]};
            // else echo '"'.$obj->{$fieldName[$i]}.'"';
            echo '"'.$obj->{$fieldName[$i]}.'"';
            if($i < $numField-1) echo ',';
          }
          echo ']';
          if($c < $result->num_rows) echo ',';
      } 
  if($ch == 1) echo ']';
}

function toArrayStringAddNumberRow($result,$ch)
{
  if($ch == 1)echo '[';
  $len = $result->num_rows;
  if($len>0)
  {
    echo '["'; 
    $row = $result->fetch_array(MYSQLI_NUM);
    array_unshift($row,1);
    echo join('","', $row);
    echo '"]';
    for($i=1;$i<$len;$i++)
    {
      echo ',["'; 
      $row = $result->fetch_array(MYSQLI_NUM);
      array_unshift($row,$i+1);
      echo join('","', $row);
      echo '"]';
    }
  }
  if($ch == 1) echo ']';
}

function toArrayStringAddNumberRowSort($result,$ch,$nSort=0)
{
  $c=$nSort;
  if($ch == 1)echo '[';
  if($result->num_rows>0) 
  {
    echo '["'; 
    $row = $result->fetch_array(MYSQLI_NUM);
    array_unshift($row,++$c);
    echo join('","', $row);
    echo '"]';
  }
  while($row = $result->fetch_array(MYSQLI_NUM))
  {
    echo ',["'; 
    array_unshift($row,++$c);
    echo join('","', $row);
    echo '"]'; 
  }
  if($ch == 1) echo ']';
}


function toArrayStringOne($result,$ch)
{
  echo '[';
  $len = $result->num_rows;
  if($len>0)
  {
    echo '"'; 
    $row = $result->fetch_array(MYSQLI_NUM);
    echo join('","', $row);
    echo '"'; 
    for($i=1;$i<$len;$i++)
    {
      echo ',"'; 
      $row = $result->fetch_array(MYSQLI_NUM);
      echo join('","', $row);
      echo '"'; 
    }
  }
  echo ']';
}

function toJsonString($result,$ch)
{
  if($ch == 1)echo '[';
    $field = 0;
    $c = 0;
      while($obj = $result->fetch_object())
      { 
        $c++;
          if($field == 0)
          {
            $fieldName = array_keys(get_object_vars($obj));
            $numField = count($fieldName);
            $field = 1;
          }
          echo '{';
          for($i=0;$i<$numField;$i++)
          {
            // if (is_numeric ($obj->{$fieldName[$i]})) echo  '"'.$fieldName[$i].'":'.$obj->{$fieldName[$i]};
            // else echo '"'.$fieldName[$i].'":'.'"'.$obj->{$fieldName[$i]}.'"';
            echo '"'.$fieldName[$i].'":'.'"'.$obj->{$fieldName[$i]}.'"';
            if($i < $numField-1) echo ',';
          }
          echo '}';
          if($c < $result->num_rows) echo ',';
      } 
  if($ch == 1) echo ']';
}

function toJsonStringAddNumberRow($result,$ch)
{
  if($ch == 1)echo '[';
    $field = 0;
    $c = 0;
      while($obj = $result->fetch_object())
      { 
        $c++;
          if($field == 0)
          {
            $fieldName = array_keys(get_object_vars($obj));
            $numField = count($fieldName);
            $field = 1;
          }
          echo '{"No":'.$c.',';
          for($i=0;$i<$numField;$i++)
          {
            // if (is_numeric ($obj->{$fieldName[$i]})) echo  '"'.$fieldName[$i].'":'.$obj->{$fieldName[$i]};
            // else echo '"'.$fieldName[$i].'":'.'"'.$obj->{$fieldName[$i]}.'"';
            echo '"'.$fieldName[$i].'":'.'"'.$obj->{$fieldName[$i]}.'"';
            if($i < $numField-1) echo ',';
          }
          echo '}';
          if($c < $result->num_rows) echo ',';
      } 
  if($ch == 1) echo ']';
}

function toJsonStringOne($result,$ch)
{
  echo '{';
    $field = 0;
    $c = 0;
      while($obj = $result->fetch_object())
      { 
        $c++;
          if($field == 0)
          {
            $fieldName = array_keys(get_object_vars($obj));
            $numField = count($fieldName);
            $field = 1;
          }
          // echo '{';
          for($i=0;$i<$numField;$i++)
          {
            // if (is_numeric ($obj->{$fieldName[$i]})) echo  '"'.$fieldName[$i].'":'.$obj->{$fieldName[$i]};
            // else echo '"'.$fieldName[$i].'":'.'"'.$obj->{$fieldName[$i]}.'"';
            echo '"'.$fieldName[$i].'":'.'"'.$obj->{$fieldName[$i]}.'"';
            if($i < $numField-1) echo ',';
          }
          // echo '}';
          if($c < $result->num_rows) echo ',';
      } 
  echo '}';
}

function toTableString($result,$ch)
{
  echo '<table class="table table-bordered">';
  $field = 0;
    $c = 0;

      while($obj = $result->fetch_object())
      { 
        $c++;
          if($field == 0)
          {
            echo '<thead><tr><th>No</th>'; 
            $fieldName = array_keys(get_object_vars($obj));
            $numField = count($fieldName);
            $field = 1;
            for($j=0;$j<$numField;$j++)
            {
              echo '<th>'.$fieldName[$j].'</th>';
            }
            echo '</tr></thead><tbody>';

          }

          echo '<tr><td>'.$c.'</td>';
          for($i=0;$i<$numField;$i++)
          {
            echo '<td>'.$obj->{$fieldName[$i]}.'</td>';
          }
          echo '</tr>';
      }
  echo '</tbody></table>';
}

function toCsvStr($result,$header)
{
  
  echo $header;
  echo "\n";
  $c= 0;
  while($row = $result->fetch_array(MYSQLI_NUM))
  {
    echo ++$c;
    echo ','; 
    echo '"'; 
    echo join('","',$row);
    echo '"'; 
    echo "\n";
  }
}

function jsonRow($result,$row=true,$seq=0) 
{ 
    $data = array(); 
    if($row) 
    { 
      $i=$seq; 
      while ($row=$result->fetch_array(MYSQLI_ASSOC)) 
      { 
        $row['NO'] = ++$i; 
        $data[] = $row; 
      } 
    } 
    else 
    { 
      while ($row=$result->fetch_array(MYSQLI_ASSOC)) 
      { 
        $data[] = $row; 
      } 
    } 
    return $data; 
}

function pageHelper($pq_curPage, $pq_rPP, $total_Records){
    $skip = ($pq_rPP * ($pq_curPage - 1));

    if ($skip >= $total_Records)
    {        
        $pq_curPage = ceil($total_Records / $pq_rPP);
        $skip = ($pq_rPP * ($pq_curPage - 1));
    }    
    return $skip;
}



?>
