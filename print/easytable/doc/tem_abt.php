<?php
include '../fpdf.php';
include '../exfpdf.php';
include '../easyTable.php';
include 'PDF_Code128.php';
include('../../../php/connection.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);
// $data = $mysqli->real_escape_string(trim(strtoupper($_REQUEST['data'])));
$data = 'Hello Test';
class PDF extends PDF_Code128
{
  function __construct($orientation='P', $unit='mm', $format='A4')
  {
    parent::__construct($orientation,$unit,$format);
        $this->AliasNbPages();
  }
  public function setHeaderData($v)
    {
      $this->headerData = $v;
    }
    public function setInstance($v)
    {
      $this->instance = $v;
    }
    function Header()
    {
      $v = $this->headerData;
      $header=new easyTable($this->instance, '%{20, 50, 30,}','border:0;font-family:THSarabun;font-size:12; font-style:B;');
        $header->easyCell('', 'img:images/abt-logo.gif, w35;align:L','');
        $header->easyCell('Head Office 336/7 Moo 7 Bowin, Sriracha
          Chonburi 20230 Thailand
          Tel. (66) 38 110 910-2, (66) 38 110 915 
          mail@albatrossthai.com', 'valign:M;align:L');
        $header->printRow();
        $header->endTable(2);
    }
    function Footer()
    {
      $this->SetXY(-20,0);
      $this->SetFont('THSarabun','I',8);
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}


$headerData = array('startdate'=>'','enddate'=>'');
$pdf=new PDF('P');
$pdf->AddFont('THSarabun','','THSarabun.php');
$pdf->AddFont('THSarabun','I','THSarabun Italic.php');
$pdf->AddFont('THSarabun','B','THSarabun Bold.php');
$pdf->AddFont('THSarabun','BI','THSarabun Bold Italic.php');
$pdf->setInstance($pdf);
$pdf->setHeaderData($headerData);


  $pdf->AddPage();
  $detail =new easyTable($pdf, '{60,60}','width:120;border:0;font-family:THSarabun;font-size:30;');
  $detail->easyCell(utf8Th($data), 'align:L;border:0;valign:M;');
  $x=$pdf->GetX();
  $y=$pdf->GetY();
  $pdf->instance->Code128($x,$y+20,('https://subaru.titan-vns.com/subaruM'),60,8);
  $pdf->instance->Code128($x,$y+40,('https://subaru.titan-vns.com/subaruM'),80,8);

  $detail->printRow();
  $detail->endTable(10);


  $pdf->Output();


function utf8Th($v)
{
  return iconv( 'UTF-8','TIS-620//TRANSLIT',$v);
}
?>